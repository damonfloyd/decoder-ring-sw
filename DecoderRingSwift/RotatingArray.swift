//
//  RotatingArray.swift
//  DecoderRingSwift
//
//  Created by Floyd, Damon on 6/8/16.
//  Copyright © 2016 Third Street Soft, LLC. All rights reserved.
//

import Foundation

extension Array {
    // rotate one element to the right.
    // [1, 2, 3] -> [3, 1, 2]
    mutating func rotateRight() -> [Element] {
        let lastElement = self.removeLast()
        self.insert(lastElement, atIndex: 0)
        return self
    }
    
    //rotate one element to the left.
    // [1, 2, 3] -> [2, 3, 1]
    mutating func rotateLeft() -> [Element] {
        let firstElement = self.removeFirst()
        self.append(firstElement)
        return self
    }
    
}
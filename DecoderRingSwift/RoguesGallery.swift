//
//  RoguesGallery.swift
//  DecoderRingSwift
//
//  Created by Floyd, Damon on 6/8/16.
//  Copyright © 2016 Third Street Soft, LLC. All rights reserved.
//

import Foundation

public class RoguesGallery {
    
    let alphabet = "abcdefghijklmnopqrstuvwxyz"
    var table: [Character: [Character : Character]]
    var keywordValid:Bool
    var tableCreated:Bool
    
    
    init() {
        self.keywordValid = false
        self.tableCreated = false
        self.table = Dictionary<Character, Dictionary<Character, Character>>()
    }
    
    func encrypt(message: String, messageKey: String) -> String {
        if(!tableCreated) {
            // alert user that table key needs 5 unique characters
            return "__TABLE_KEY_TOO_SHORT__"
        }
        else {
            var keyIndex = messageKey.startIndex
            let lastIndex = messageKey.endIndex.advancedBy(-1)
            var plaintext = "";
            for c in message.characters {
                let lc = String(c).lowercaseString.characters.first!
                if self.alphabet.indexOfCharacter(lc) == nil {
                    plaintext.append(c)
                }
                else {
                    let keyChar = messageKey[keyIndex]
                    var decrypted = self.table[lc]![keyChar]!
                    // if original character wasn't lowercase, make it uppercase again
                    if(c != lc) {
                        decrypted = String(decrypted).uppercaseString.characters.first!
                    }
                    plaintext.append(decrypted)
                    if(keyIndex < lastIndex) {
                        keyIndex = keyIndex.advancedBy(1)
                    } else {
                        keyIndex = messageKey.startIndex
                    }
                }
            }
            return plaintext
        }
    }
    
    func generateTable(key: String) {
        var preparedKey = key
        preparedKey.stripDupes()
        if(preparedKey.characters.count >= 5) {
            let rowsColumns = completeLetterSet(preparedKey)
            var alphabet = completeLetterSet(preparedKey)
            alphabet.reverse()
            for c in rowsColumns.characters {
                var idx = alphabet.startIndex
                var row = [Character: Character]()
            
                for r in rowsColumns.characters {
                    row.updateValue(alphabet.characters[idx], forKey: r)
                    idx = idx.advancedBy(1)
                }
                table.updateValue(row, forKey: c)
                alphabet.rotateLeft()
            }
            self.tableCreated = true
        }
        else {
            self.tableCreated = false
        }
    }
    
    func invalidateTable() {
        self.tableCreated = false
    }
    
    func completeLetterSet(key: String) -> String {
        let keyLength = key.characters.count
        let alphaLength = 26 - keyLength
        if(alphaLength < 1) {
            return ""
        }
        
        var outString = key
        for alpha in alphabet.characters {
            var cStr = ""
            cStr.append(alpha)
            if outString.rangeOfString(cStr) == nil {
                outString.append(alpha)
            }
        }
        
        return outString
    }
    
    func rotateLeft(str: String) -> String {
        let first = str.characters.first!
        var outstr = String(str.characters.dropFirst(1))
        outstr.append(first)
        return outstr
    }
}


//
//  StringExtension.swift
//  DecoderRingSwift
//
//  Created by The Monk on 6/9/16.
//  Copyright © 2016 Third Street Soft, LLC. All rights reserved.
//

import Foundation

public extension String {
    public mutating func rotateLeft() -> String {
        let first = self.characters.first!
        self = String(self.characters.dropFirst(1))
        self.append(first)
        return self
    }
    
    public mutating func rotateRight() -> String {
        let last = self.characters.last!
        self = String(self.characters.dropLast())
        self.insert(last, atIndex: self.startIndex)
        return self
    }
    
    public mutating func reverse() -> String {
        var reversed = ""
        for char in self.characters {
            let asString = "\(char)"
            reversed = asString + reversed
        }
        self = reversed
        return self
    }
    
    public func indexOfCharacter(char: Character) -> Int? {
        if let idx = self.characters.indexOf(char) {
            return self.startIndex.distanceTo(idx)
        }
        return nil
    }
    
    public mutating func stripDupes() -> String {
        var found = ""
        let lcStr = self.lowercaseString
        for char in lcStr.characters {
            var cstr = ""
            cstr.append(char)
            if found.lowercaseString.rangeOfString(cstr) == nil {
                found.append(char)
            }
        }
        self = found
        return self
    }

    public func containsOnlyCharactersIn(matchChars: String) -> Bool {
        let disallowedChars = NSCharacterSet(charactersInString: matchChars).invertedSet
        return self.rangeOfCharacterFromSet(disallowedChars) == nil
    }
    
}

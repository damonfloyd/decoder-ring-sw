//
//  StoreContact.swift
//  DecoderRingSwift
//
//  Created by Floyd, Damon on 6/22/16.
//  Copyright © 2016 Third Street Soft, LLC. All rights reserved.
//

import Foundation
import CoreData

class StoreContact: NSManagedObject {
    @NSManaged var name: String
    @NSManaged var phone: String
    
    class func createInManagedObjectContext(moc: NSManagedObjectContext, _name: String, _phone: String) -> StoreContact {
        let newContact = NSEntityDescription.insertNewObjectForEntityForName("StoreContact", inManagedObjectContext: moc) as! StoreContact
        
        newContact.name = _name
        newContact.phone = _phone
        
        return newContact
    }
}
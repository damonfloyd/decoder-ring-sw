//
//  ViewController.swift
//  DecoderRingSwift
//
//  Created by Floyd, Damon on 6/8/16.
//  Copyright © 2016 Third Street Soft, LLC. All rights reserved.
//

import UIKit
import Foundation
import Contacts
import ContactsUI


class ViewController: UIViewController, UITextFieldDelegate, CNContactPickerDelegate {

    // MARK: Properties
    @IBOutlet weak var txtTableKey: UITextField!
    @IBOutlet weak var txtMessageKey: UITextField!
    @IBOutlet weak var btnTranslate: UIButton!
    @IBOutlet weak var btnClear: UIButton!
    @IBOutlet weak var tvMessageText: UITextView!
    @IBOutlet weak var txtContactName: UITextField!
    @IBOutlet weak var bbtnContacts: UIBarButtonItem!
    @IBOutlet weak var bbtnCompose: UIBarButtonItem!
    @IBOutlet weak var tbToolbar: UIToolbar!
    
    var rg = RoguesGallery()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        txtTableKey.delegate = self
        self.navigationController?.navigationBarHidden = true
        
        setupButtons()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
    // MARK: UI Tweaks
    func setupButtons() {
        btnTranslate.layer.cornerRadius = 5
        btnTranslate.layer.borderColor = UIColor.whiteColor().CGColor
        btnTranslate.layer.borderWidth = 1
        
        btnClear.layer.cornerRadius = 5
        btnClear.layer.borderColor = UIColor.whiteColor().CGColor
        btnClear.layer.borderWidth = 1
    }
    
    func makeToolbarTransparent() {
        let backgroundImage = UIImage(named: "TransBack")
        tbToolbar.setBackgroundImage(backgroundImage, forToolbarPosition: UIBarPosition.Any, barMetrics: UIBarMetrics.Default)
    }
    
    // MARK: Button handlers
    @IBAction func translateMessage(sender: UIButton) {
        // do the thing
        if let msgKey = txtMessageKey.text {
            if msgKey.characters.count < 2 {
                AppDelegate.getAppDelegate().showMessage("Message Key should be at least 2 characters!")
            }
            else {
                if rg.tableCreated {
                    tvMessageText.text = rg.encrypt(tvMessageText.text, messageKey: txtMessageKey.text!)
                }
            }
        }
    }
    
    @IBAction func clearAllFields(sender: UIButton) {
        tvMessageText.text = "";
    }
    
    @IBAction func pickContact(sender: UIBarButtonItem) {
        let contactPickerViewController = CNContactPickerViewController()
        contactPickerViewController.delegate = self
        contactPickerViewController.predicateForEnablingContact = NSPredicate(format: "phoneNumbers.@count > 0", argumentArray: nil)
        contactPickerViewController.predicateForSelectionOfProperty = NSPredicate(format: "key == 'phoneNumbers'", argumentArray: nil)
        
        
        presentViewController(contactPickerViewController, animated: true, completion: nil)
    }
    
    
    // MARK: UITextFieldDelegate stuff
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        view.endEditing(true)
        super.touchesBegan(touches, withEvent: event)
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return false
    }
    
    func textFieldDidEndEditing(textField: UITextField) {
        if textField.isEqual(txtTableKey) {
            if var newKey = textField.text {
                newKey.stripDupes()
                if newKey.characters.count < 5 {
                    AppDelegate.getAppDelegate().showMessage("Table Key must have 5 UNIQUE letters!")
                    rg.invalidateTable()
                }
                else {
                    rg.generateTable(newKey)
                }
            }
        }
    
    }
    
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        
        // jump out if we have erased errrything
        guard string.characters.count > 0 else {
            return true;
        }
        
        // force lowercase letters only for table/message keys
        let allowed = "abcdefghijklmnopqrstuvwxyz"
        if(textField.isEqual(txtTableKey) || textField.isEqual(txtMessageKey)) {
            let currentText = textField.text ?? ""
            let proposedText = (currentText as NSString).stringByReplacingCharactersInRange(range, withString: string)
            return proposedText.containsOnlyCharactersIn(allowed)
        }
        
        return false
    }
    
    // MARK:  Contact Picker stuff
    
    func contactPicker(picker: CNContactPickerViewController, didSelectContactProperty contactProperty: CNContactProperty) {
        print((contactProperty.value as! CNPhoneNumber).valueForKey("digits")!)
        var name = contactProperty.contact.nickname ?? ""
        if name == ""  {
            name = "\(contactProperty.contact.givenName) \(contactProperty.contact.familyName)"
        }
        let phone = (contactProperty.value as! CNPhoneNumber).valueForKey("digits") as! String
//        let newContact = StoreContact.createInManagedObjectContext, _name: <#T##String#>, _phone: <#T##String#>
        //txtContactName.text = newContact.name
    }
}

